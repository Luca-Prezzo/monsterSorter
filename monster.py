from PIL import Image


class Monster:
    """Monster Class for sorting the can based on color"""

    def __init__(self, file):
        self.file = file
        self.frame = self.get_frame()
        self.avarage = round(self.get_avarage(), 2)

    def __str__(self):
        return f"file {self.file}, avarage_point: {self.avarage}"

    def __repr__(self):
        return f"Monster({self.file})"

    # carico la foto, la converto
    # in bianco e nero e salvo la lista dei pixel
    def get_frame(self) -> list:
        """:return: list of pixel"""
        image = Image.open(self.file)
        resized = image.resize((500, 1245))
        converted = resized.convert("RGBA")
        return list(converted.getdata())

    # prendo il valore medio dalla lista dei pixel
    def get_avarage(self) -> float:
        """:return: avarage value of pixel brightness"""
        frame = [sum(pixel) for pixel in self.frame if pixel != (0, 0, 0)]
        total_pixel_value = sum(frame)
        return total_pixel_value / len(frame)
