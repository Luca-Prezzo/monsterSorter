# Ordinatore di lattine Monster

Lo scopo dietro questo programma python è quello di ottenere la luminosità di ogni lattina Monster per poter ordinare la mensola.

Fotografo tutte le lattine, ritaglio le foto e le ridimensiono per avere sepre la stessa dimensione.

Tramite la libreria `Pillow` vado a caricare la foto nello script di python e converto la foto da `RGBA` a `L` (in bianco e nero) ed estraggo l'array bidimensionale che contiene la lista dei pixel.

Sommo tutti i valori dei pixel e divido per la lunghezza dell'array ottenendo così un valore medio che possa indicarmi la luminosità dell'immagine.

Ora che ho la lista completa delle foto con il relativo valore medio di luminosità posso andare a ordinare la lista.