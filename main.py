import os
import shutil

from monster import Monster


def main():

    # se non esiste la cartella per
    # le immagini originali, esco
    if not os.path.exists("unsorted"):
        raise FileNotFoundError("Impossibile trovare la cartella 'unsorted' contenente le immagini da ordinare")


    # se non esiste la cartella dove
    # mettere i file ordinati, la creo
    if not os.path.exists("sorted"):
        print("Creo la cartella 'sorted' per le immagini ordinate")
        os.mkdir("sorted")


    # creo la lista contenente tutte le istanze "Monster"
    file = [Monster(f"unsorted/{file}") for file in os.listdir("unsorted")]


    # ordino la lista in base al valore medio di luminosità
    valori = [(foto.file, foto.avarage) for foto in file]
    valori.sort(key=lambda x: x[1])

    # sposto i file nella cartella riordinando i nomi
    for index, val in enumerate(valori, start=1):
        print(f"[ - {index} - ] File: {val[0]} - Punteggio luminosità: {val[1]}")
        shutil.copy(val[0], f"sorted/{index}.png")


if __name__ == '__main__':
    main()
